resource "aws_key_pair" "kubespray" {
  key_name   = "kubespray"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDvy9n3t+q8t2wd48fxqfFQPXO6+5dQWPsh1mcl57v3vouB+C+p9vNtTNQa4wXUq4clsPsgf4h7vxYkf606ezu0Hg50F9ZAm2KtNFtsvfIj6QwVTH8D0k2PUAiYQZpzKZD6FUeADLLpYNl/+qdpkSH9+beuiU5y7ThLT/D0Hd8uMndNUzFa3sxy79TDDYqwhFiJPOD6gWhpItjYLIcs4DYmAUwMXlhV0ejebXDk0pBgA/+WEnV+ycPnS22aqpIO9E++GUIDzwZI7UXSbwxJsZQJWAYSZ3lFJ+VKnMk22bhgH7wqLzPl6THgjK6lv3daybIYT4Z7HMt351kxFiKCDAYRdFe90feLIGyyRvZMClp9b6vfoNZ9SHq1qhQ/Vv83mA8EgG7cM7nhJV+J4tYmbgKcYRgZI984pJtD9c3hEK2ndLxN+o2x/iK0Mpop9Yf703rvvMsMQ+44YrVYTncnylazzOJA5Pwk13bjCYNceRWiXyKxlgop5FTKL2cuvH+dYyc= ubuntu"

}

module "vm" {
  depends_on    = [aws_key_pair.kubespray]
  source        = "../modules/vm"
  vm_name       = "master-${count.index}"
  instance_type = "t3.large"
  key_name      = aws_key_pair.kubespray.key_name
  count         = 4
}

output "master-ips" {
  value = {
    for inst in module.vm : inst.name => inst.ip
  }
}
