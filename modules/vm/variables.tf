variable "vm_name" {
  description = "tag to name the vm"
  type        = string
}

variable "key_name" {
  description = "title of the provisioning ssh key"
  type        = string
}

variable "instance_type" {
  description = "aws instance type"
  type        = string
}

