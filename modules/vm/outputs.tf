output "ip" {
  value = aws_instance.web.public_ip
}

output "name" {
  value = aws_instance.web.tags.Name
}
