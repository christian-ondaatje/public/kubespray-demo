#Kubespray Demo
### Christian Ondaatje


# development

1. configure aws:

```
export AWS_ACCESS_KEY_ID="$(gpg --decrypt < "secrets/aws.secret" | sed -n 1p)"
export AWS_SECRET_ACCESS_KEY="$(gpg --decrypt < "secrets/aws.secret" | sed -n 2p)"
```

# usage

1. `source spray-env/bin/activate` (or do a fresh virtualenv for 
`pip3 install -r kubespray/requirements.txt`)


2. `terragrunt init && cd cluster && terragrunt apply` (note that you'll need
some special firewalling etc that's out of scope here)


3. cd into kubespray dir and run

`cp -rfp inventory/sample inventory/mycluster`

`declare -a IPS=(34.244.186.170 34.241.237.244)` (replace with your IPs of course)

`CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}`


4. edit `inventory/mycluster/hosts.yaml` with `ansible_host` as the public IP,
`ip` and `access_ip` as the private IP of each node

5. run `ansible-playbook -i inventory/mycluster/hosts.yaml -u=ubuntu --private-key=../kubespray.key.secret  --become --become-user=root cluster.yml`







